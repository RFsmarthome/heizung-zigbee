import adbase as ad
import hassapi as hass
import mqttapi as mqtt
import json as json
import requests as requests
import datetime as datetime
import time as time
import traceback as traceback
from influxdb_client import InfluxDBClient
import numpy as np



class HeizungZigbee(ad.ADBase):

    RUN_EVERY15 = 15

    def initialize(self):
        self.hass = self.get_plugin_api("HASS")
        self.mqtt = self.get_plugin_api("MQTT")
        self.adbase = self.get_ad_api()

        self.adbase.log("Init HeizungZigbee")

        self.initConfig()

        self.initInfluxDB()
        self.initDevices()



    def initConfig(self):
        self.fuzzyApi = self.args["fuzzy_api"]
        self.adbase.log("FuzzyAPI - %s" % self.fuzzyApi)

        self.topic = self.args["topic"]
        self.fuzzy_name = self.args["fuzzy_name"]
        self.hass_name = self.args["hass_name"]
        self.temp_name = self.args["temp_name"]
        self.temp_slope = self.args["temp_slope"]
        self.hass_valve_name = self.args["hass_valve_name"]
        self.hass_battery_name = self.args["hass_battery_name"]
        self.past = self.args["backtime"]
        self.fuzzy_timer = self.args["fuzzy_timer"]
        self.valve = 0

        self.last_check = time.time()
        self.last_send_valve = time.time()-6000 # Last valve send only init...
        self.old_valve = 0


    def initInfluxDB(self):
        self.influx_url = self.args["influx"]["url"]
        self.influx_token = self.args["influx"]["token"]
        self.influx_org = self.args["influx"]["org"]
        self.influx_bucket = self.args["influx"]["bucket"]
        self.influx_ssl = self.args["influx"]["ssl"]
        self.adbase.log("InfluxDB - %s (token: %s, org: %s, bucket: %s, ssl: %s)" % (self.influx_url, self.influx_token, self.influx_org, self.influx_bucket, self.influx_ssl))

        self.influx_client = None
        self.openInfluxClient()



    def initDevices(self):
        self.adbase.log("Init device", level="INFO")

        # Call fuzzy calculations
        self.adbase.run_every(self.runFuzzy, "now+15", self.fuzzy_timer)

        # Timer for query
        self.adbase.run_every(self.runRequestConfig, "now", self.RUN_EVERY15*60)

        # Timers for valve maintance
        self.adbase.run_daily(self.runTo100, datetime.time(14,3,18))
        self.adbase.run_daily(self.runTo0, datetime.time(14,4,18))

        self.subscribeMqtt()
        self.sendEnableTrvMode()

        #self.readFromInfluxFit(self.temp_name, self.past)



    def runTo100(self):
        if self.days > 5:
            self.valve = 100

    def runTo0(self):
        if self.days > 5:
            self.days = 0
            self.valve = 0
        else:
            self.days += 1



    def openInfluxClient(self):
        self.adbase.log("Open influx client", level="DEBUG")
        if self.influx_client:
            self.adbase.log("Close before reopen", level="DEBUG")
            self.influx_client.close()

        self.influx_client = InfluxDBClient(url=self.influx_url, token=self.influx_token, org=self.influx_org, verify_ssl=self.influx_ssl)



    def readFromInflux(self, tempSensorName, past):
        query =u'from(bucket: "%s") |> range(start: %s) |> filter(fn: (r) => r._measurement=~/.C$/ and r._field=="value" and r.entity_id=="%s") |> first()' % (self.influx_bucket, past, tempSensorName)

        self.adbase.log("Query is "+ query, level="DEBUG")
        query_api = self.influx_client.query_api()
        try:
            self.adbase.log("First try", level="DEBUG")
            result = query_api.query(query)
        except Exception:
            self.adbase.log("Second try", level="DEBUG")
            self.openInfluxClient()
            result = query_api.query(query)

        for table in result:
            value = float(table.records[0]["_value"])
        self.adbase.log("Query result is %f" % value)
        return value

    def readFromInfluxFit(self, tempSensorName, past):
        query =u'from(bucket: "%s") |> range(start: %s) |> filter(fn: (r) => r._measurement=~/.C$/ and r._field=="value" and r.entity_id=="%s")' % (self.influx_bucket, past, tempSensorName)

        self.adbase.log("Query is %s" % query, level="DEBUG")
        query_api = self.influx_client.query_api()
        try:
            self.adbase.log("First try", level="DEBUG")
            result = query_api.query(query)
        except Exception:
           self.adbase.log("Second try", level="DEBUG")
           self.openInfluxClient()
           result = query_api.query(query)

        table = result[0]
        times = np.array([])
        values = np.array([])
        for record in table.records:
            times = np.append(times, datetime.datetime.timestamp(record["_time"]))
            values = np.append(values, record["_value"])

        fit = np.polyfit(times, values, 1)
        poly = np.polynomial.Polynomial.fit(times, values, 1)
        self.adbase.log("Slope: %f" % poly.convert().coef[1], level="DEBUG")
        return poly.convert().coef[1]

    def mqttCallback(self, event_name, data, kwargs):
        self.adbase.log("Event_Name: %s" % event_name, level="DEBUG")
        self.adbase.log("DataPayloads in RAW:", level="DEBUG")
        self.adbase.log(data, level="DEBUG")

        payload = json.loads(data["payload"])
        try:
            self.trv_mode = int(payload["trv_mode"])
            #self.valve = int(payload["valve_position"])
            self.adbase.log("Get trv_mode: %d" % self.trv_mode, level="DEBUG")

            if self.trv_mode!=1:
                now_time = time.time()

                if now_time - self.last_check > 15:
                    self.last_check = time.time()
                    self.adbase.log("Enable TRV_MODE 1")
                    self.sendEnableTrvMode()

                    self.send_get_valve = True
                    self.send_get_trv_mode = True
                    self.sendCycle()

        except Exception:
            self.adbase.log("Catch exception, try full cycle on device", level="DEBUG")
            self.send_get_valve = True
            self.send_get_trv_mode = True
            self.sendCycle()



    def runRequestConfig(self, kwargs):
        self.adbase.log("Timer every 15min", level="DEBUG")

        self.send_get_valve = True
        self.send_get_trv_mode = True



    def runFuzzy(self, kwargs):
        self.adbase.log("Fuzzy timer", level="DEBUG")

        self.sendCycle()

        climateEntity = self.hass.get_state(self.hass_name, "all")
        self.adbase.log("Climate HASS attributes:", level="DEBUG")
        self.adbase.log(climateEntity, level="DEBUG")
        current_temperature = float(climateEntity["attributes"]["current_temperature"])
        request_temperature = float(climateEntity["attributes"]["temperature"])

        history_temperature = self.readFromInflux(self.temp_name, self.past)

        # Set slope to HASS
        slope = self.readFromInfluxFit(self.temp_name, self.past)
        self.hass.set_state(self.temp_slope, state = slope)

        oldValve = self.valve

        self.adbase.log("Current Temperature: %f" % current_temperature, level="DEBUG")
        self.adbase.log("Requested Temperature: %f" % request_temperature, level="DEBUG")
        self.adbase.log("History Temperature: %f" % history_temperature, level="DEBUG")

        if climateEntity["state"] == "off":
            self.adbase.log("Heating is off", level="DEBUG")
            self.valve = 0
        else:
            requestUrl = "%s/valve_correction/%s" % (self.fuzzyApi, self.fuzzy_name)
            requestParams = {
                "current": current_temperature-request_temperature,
                "past": current_temperature-history_temperature
            }
            self.adbase.log("Request %s with params %s" % (requestUrl, requestParams), level="DEBUG")
            resp = requests.get(requestUrl, params=requestParams)
            if resp.status_code == requests.codes.okay:
                self.adbase.log("Response text: %s" % resp.text)
                fuzzy = json.loads(resp.text)
                if fuzzy["valveCorrection"]=="NaN":
                    self.adbase.log("Valve correction will be %f" % (request_temperature - current_temperature))
                    self.addValve(request_temperature - current_temperature)
                else:
                    self.adbase.log("Valve correction is %f" % float(fuzzy["valveCorrection"]))
                    self.addValve(float(fuzzy["valveCorrection"]))
            else:
                self.adbase.log("Error by fuzzy request: %d (%s)" % (resp.status_code, resp.text), level="WARN")

        self.adbase.log("Old valve: %f -> new Valve: %f" % (oldValve, self.valve), level="INFO")
        self.sendSetValve()
        self.adbase.log("Device %s set state to %f" % (self.hass_valve_name, self.valve), level="DEBUG")
        #self.hass.set_value(self.hass_valve_name, self.valve)
        self.hass.set_state(self.hass_valve_name, state = self.valve)



    def addValve(self, valve):
        self.valve += valve
        if self.valve < 0:
            self.valve = 0
        if self.valve > 255:
            self.valve = 255

    def subscribeMqtt(self):
        self.mqtt.call_service("mqtt/subscribe", topic=self.topic, qos=2)
        self.mqtt.listen_event(self.mqttCallback, "MQTT_MESSAGE", topic=self.topic)

    def sendCycle(self):
        if self.send_get_valve:
            self.send_get_valve = False
            self.sendGetValve()

        if self.send_get_trv_mode:
            self.send_get_trv_mode = False
            self.sendGetTrvMode()

    def sendGetValve(self):
        return self.publish('{"valve_position": ""}')

    def sendGetTrvMode(self):
        return self.publish('{"trv_mode": ""}')

    def sendEnableTrvMode(self):
        return self.publish('{"trv_mode": 1}', type="set")

    def sendEnableChildProtection(self):
        return self.publish('{"child_protection": true}')

    def sendSetValve(self):
        v = int(self.valve)
        now_time = time.time()
        if v != self.old_valve:
            self.last_send_valve = now_time
            self.old_valve = v
            return self.publish('{"valve_position": "%d"}' % v, type="set")
        else:
            if self.last_send_valve < now_time-60*15: # Repeat valve every 15 min
                self.last_send_valve = now_time
                self.old_valve = v
                return self.publish('{"valve_position": "%d"}' % v, type="set")

    def publish(self, payload, type="get"):
        return self.mqtt.call_service("mqtt/publish", topic="%s/%s" % (self.topic, type), payload=payload)
